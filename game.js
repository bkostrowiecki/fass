var inputState = {
	mouse: { 'x': 0, 'y': 0, 'down': 0, 'up': 0 }
}

/** SoundPool class **/
function SoundPool ( num, base ) {
	this.playedIndex = 0;
	this.sounds = [];
	for ( var i=0; i<num; i++ ) {
		this.sounds[i] = new Audio(  base[ parseInt( Math.random() * base.length ) ] );
	}
}

SoundPool.prototype.play = function() {
	this.sounds[this.playedIndex].play();
	this.playedIndex++;
	if ( this.playedIndex >= this.sounds.length ) {
		this.playedIndex = 0;
	}
}

/** End of SoundPool class **/

/** Explosion class **/
function Explosion( x, y, color ) {
	this.particles = [];
	for ( var i=0; i < 2*Math.PI; i += 2*Math.PI / 15 ) {
		this.particles[i] = { 'x': x, 'y': y, 'dirx': Math.cos(i)*3, 'diry': Math.sin(i)*3, 'r': 6 };
	}
	this.color = color;
	this.dead = 0;
	this.start = new Date().getTime();
}

Explosion.prototype.spread = function( context ) {
	for ( var i=0; i < 2*Math.PI; i += 2*Math.PI / 15 ) {
		context.beginPath();
		context.arc( this.particles[i].x, this.particles[i].y, this.particles[i].r, 0, 2 * Math.PI, false );
		context.fillStyle = this.color;
		context.fill();
		this.particles[i].x += this.particles[i].dirx;
		this.particles[i].y += this.particles[i].diry;
		if ( this.particles[i].r>0.1 ) { 
			this.particles[i].r-=0.1;
		} else {
			this.dead = true;
			return false;
		}
	}
}
/** End of Explosion class **/

/** Scene class **/
function Scene( canvas ) {
	this.canvas = canvas;
	this.context = this.canvas.getContext('2d');
}
/** End of Scene class **/

/** Game class **/
function Game( config ) {
	this.canvas = document.getElementById( config.canvasName.toString() );
	this.canvas.style.cursor = 'none';
	this.context = this.canvas.getContext('2d');
	this.width = config.width;
	this.height = config.height;
	this.currentScene = undefined;
	this.fps = 60;
	this.canvas.addEventListener( 'mousemove', this.onMousemove );
	this.canvas.addEventListener( 'mousedown', this.onMousedown );
	this.canvas.addEventListener( 'mouseup', this.onMouseup );
}

Game.prototype.setCurrentScene = function( scene ) {
	if ( this.currentScene!==undefined ) {
		this.currentScene.onStop();
	}
	this.currentScene = scene;
	this.currentScene.onStart();
}

Game.prototype.onMousemove = function( e ) {
	e.preventDefault();
	inputState.mouse.x = e.pageX - canvas.offsetLeft;
	inputState.mouse.y = e.pageY - canvas.offsetTop;
}

Game.prototype.onMousedown = function() {
	inputState.mouse.down = 1;
}

Game.prototype.onMouseup = function() {
	inputState.mouse.down = 0;
}

function collision( a, b ) {
	var distX = a.x - b.x;
	var distY = a.y - b.y;
	if ( distX < 0 ) distX = -distX;
	if ( distY < 0 ) distY = -distY;
	var distance = Math.sqrt( distX*distX + distY*distY );
	if ( distance < a.r + b.r ) {
		return true;
	}
	return false;
}

Game.prototype.start = function() {

	var play = new Scene( this.canvas );

	var startInterval = 1000;

	var gunsFunc = {
		'M': function( context ) {
			context.playerBullets.push({ 'x': context.player.x - 10, 'y': context.player.y, 'r': context.guns['M'].r, 'type': 'M' });
			context.playerBullets.push({ 'x': context.player.x + 10, 'y': context.player.y, 'r': context.guns['M'].r, 'type': 'M' });
		},
		'R': function( context ) {
			if (context.lastBulletDir=='left') {
				context.playerBullets.push({ 'x': context.player.x + 10, 'y': context.player.y, 'r': context.guns['R'].r, 'type': 'R' });
				context.lastBulletDir = 'right';
			} else {
				context.playerBullets.push({ 'x': context.player.x - 10, 'y': context.player.y, 'r': context.guns['R'].r, 'type': 'R' });
				context.lastBulletDir = 'left';
			}
			context.tankfirePool.play();
		}
	}

	play.stars = [];
	play.shootTimer = new Date().getTime();
	play.playerBullets = [];
	play.enemies = [];
	play.explosions = [];
	play.player = { 
		'x': 0, 
		'y': 0, 
		'r': 12, 
		'lives': 2, 
		'dead': false, 
		'gun': 'R', 
		'lastBulletDir': 'left', 
		'score': 0,
		gunFunc: function( context ) {
			gunsFunc[ this.gun ]( context );
		}
	};
	play.enemyTimer = new Date().getTime();
	play.enemyTimerInterval = startInterval;
	play.guns = [];
	play.guns['M'] = { 
		'speed': 8, 
		'power': 25, 
		'r': 2, 
		'interval': 200 
	};
	play.guns['R'] = { 
		'speed': 5, 
		'power': 100, 
		'r': 5, 
		'interval': 350 
	};
	play.respawnTimer = 0;
	play.bonuses = [];
	play.bonusesList = [ 'M', 'R' ];

	play.tankfirePool = new SoundPool( 4, [ 'assets/tankfire.wav' ] );
	play.blastPool = new SoundPool( 20, [ 'assets/blast.wav' ] );
	play.metalPool = new SoundPool( 20, [ 'assets/metalpong.wav', 'assets/metalanvil.wav' ] );

	play.onStart = function() {
		for ( var i=0; i<500; i++ ) {
			this.stars[i] = { 
				'x': Math.floor( ( Math.random() * 800 ) + 1 ), 
				'y': Math.floor( ( Math.random() * 600 ) + 1 ), 
				'r': Math.floor( ( Math.random() * 3 ) + 1) 
			};
		}
	};

	play.onStop = function() {
	};

	play.onStep = function() {
		/** TEST ENEMIES **/
		if ( this.enemyTimer + this.enemyTimerInterval < new Date().getTime() ) {
			this.enemies.push( { 
				'x': Math.floor( ( Math.random() * 800 ) + 1 ), 
				'y': -50, 'r': 25, 
				'speed':  Math.floor( ( Math.random() * 5 ) + 1 ), 
				'live': 100 
			} );
			this.enemyTimer = new Date().getTime();
		}

		/** BACKGROUND RENDERING **/
		for ( var i=0; i<500; i++ ) {
			this.context.beginPath();
			this.context.arc( this.stars[i].x, this.stars[i].y, 1, this.stars[i].r, 2 * Math.PI, false );
			this.context.fillStyle = '#fff';
			this.context.fill();
			this.stars[i].y += 2;
			if ( this.stars[i].y > 600 ) {
				this.stars[i] = { 
					'x': Math.floor( ( Math.random() * 800 ) + 1 ), 
					'y': 0, 
					'r': Math.floor( ( Math.random() * 600 ) + 3 ) 
				}; 
			}
		}

		/** ENEMY RENDERING, ENEMY WITH PLAYER COLLISION **/
		for ( var i=0; i<this.enemies.length; i++ ) {
			this.context.beginPath();
			this.context.arc( this.enemies[i].x, this.enemies[i].y, 12, 0, 2 * Math.PI, false );
			this.context.fillStyle = '#f00';
			this.context.fill();
			this.context.lineWidth = 5;
			this.context.strokeStyle = '#330000';
			this.context.stroke();
			this.enemies[i].y+=this.enemies[i].speed;
			
			if ( collision( this.player, this.enemies[i] ) && this.player.dead!==true) {
				this.explosions.push( new Explosion( this.enemies[i].x, this.enemies[i].y, '#f00') );
				this.enemies.splice( i, 1 );
				this.explosions.push( new Explosion( this.player.x, this.player.y, '#0f0') );
				this.player.dead = true;
				this.respawnTimer = new Date().getTime();
				this.blastPool.play();
			} else if ( this.enemies[i].y > 800) {
				this.enemies.splice(i, 1);
			}
		}

		/** BULLET RENDERING, ENEMY WITH BULLET COLLISION **/
		var bulletsToDelete = [];
		for ( var i=0; i<this.playerBullets.length; i++ ) {
			this.context.beginPath();
			this.context.arc( this.playerBullets[i].x, this.playerBullets[i].y, this.playerBullets[i].r, 0, 2 * Math.PI, false );
			this.context.fillStyle = '#0f0';
			this.context.fill();
			this.playerBullets[i].y -= this.guns[ this.playerBullets[i].type ].speed;
			for ( var j=0; j<this.enemies.length; j++ ) {
				if ( collision( this.playerBullets[i], this.enemies[j] ) ) {
					this.enemies[j].live -= this.guns[ this.playerBullets[i].type ].power;
					this.metalPool.play();
					bulletsToDelete.push( i );
					this.context.beginPath();
					this.context.arc( this.enemies[j].x, this.enemies[j].y, 12, 0, 2 * Math.PI, false );
					this.context.fillStyle = '#ff0';
					this.context.fill();
					this.context.lineWidth = 5;
					this.context.strokeStyle = '#333300';
					this.context.stroke();
				}
				if (this.enemies[j].live <= 0) {
					if ( ( Math.random() * 25 ) > 21) {
						this.bonuses.push( { 'x': this.enemies[j].x, 
							'y': this.enemies[j].y,
							'r': 12,
							'speed': this.enemies[j].speed,
							'type': this.bonusesList[ Math.floor( Math.random() * this.bonusesList.length) ] 
						} );
					}
					this.explosions.push( new Explosion( this.enemies[j].x, this.enemies[j].y, '#f00') );
					this.enemies.splice( j, 1 );
					this.player.score++;
					if ( this.enemyTimerInterval > 250 ) {
						this.enemyTimerInterval-=100;
					}
					this.blastPool.play();
				}
			}
			if ( this.playerBullets[i].y < -10) {
				bulletsToDelete.push(i);
			}
		}

		/** DELETING BULLETS **/
		for ( var i=0; i<bulletsToDelete.length; i++ ) {
			this.playerBullets.splice( bulletsToDelete[i], 1 );
		}
		while ( bulletsToDelete.length > 0 ) {
			bulletsToDelete.pop();
		}

		/** DELETING EXPLOSIONS **/
		for ( var i=0; i<this.explosions.length; i++ ) {
			if ( this.explosions[i].spread( this.context )===false ) {
				this.explosions.slice(i, 1);
			}
		}

		/** BONUSES RENDERING, DETECTING COLLISION BEETWEN BONUSES AND PLAYER **/
		var bonusesToDelete = [];
		for ( var i=0; i<this.bonuses.length; i++ ) {
			this.bonuses[i].y += this.bonuses[i].speed;
			this.context.beginPath();
			this.context.arc( this.bonuses[i].x, this.bonuses[i].y, this.bonuses[i].r, 0, 2 * Math.PI, false );
			this.context.fillStyle = '#fff';
			this.context.fill();
			this.context.lineWidth = 5;
			this.context.strokeStyle = '#ccc';
			this.context.stroke();
			this.context.font = 'bold 20pt Courier New';
			this.context.fillStyle = '#333';
    	this.context.fillText( this.bonuses[i].type, this.bonuses[i].x - 8, this.bonuses[i].y + 8 );
			if ( collision( this.bonuses[i], this.player ) ) {
				this.player.gun = this.bonuses[i].type;
				this.player.gunFunc = gunsFunc[ this.player.gun ];
				bonusesToDelete.push(i);
			} else if ( this.bonuses[i].y > 650 ) {
				bonusesToDelete.push(i);
			}
		}

		/** DELETING TAKEN BONUSES **/
		for ( var i=0; i<bonusesToDelete.length; i++ ) {
			this.bonuses.splice( bonusesToDelete[i], 1 );
		}
		while ( bulletsToDelete.length > 0 ) {
			bonusesToDelete.pop();
		}

		/** PLAYER LOGIC **/
		if (this.player.dead===false) {
			// Setting player position by mouse
			this.player.x = inputState.mouse.x;
			this.player.y = inputState.mouse.y;
	
			// Detecting mouse button down, shooting
			if ( inputState.mouse.down===1 ) {
				if ( play.shootTimer + this.guns[ this.player.gun ].interval < new Date().getTime() ) {
					this.player.gunFunc( this );
					this.shootTimer = new Date().getTime();
				}
			}
			// Rendering player
			this.context.beginPath();
			this.context.arc( this.player.x, this.player.y, this.player.r, 0, 2 * Math.PI, false );
			this.context.fillStyle = '#0f0';
			this.context.fill();
			this.context.lineWidth = 5;
			this.context.strokeStyle = '#030';
			this.context.stroke();
		} else {
			// Rendering death circle
			this.context.beginPath();
			this.context.arc( inputState.mouse.x, inputState.mouse.y, this.player.r, 0, 2 * Math.PI, false );
			this.player.x = inputState.mouse.x;
			this.player.y = inputState.mouse.y;
			this.context.fillStyle = 'rgba(0,0,0,0.5)';
			this.context.fill();
			this.context.lineWidth = 5;
			this.context.strokeStyle = 'rgba(96,96,96,0.5)';
			this.context.stroke();

			// Timers for respawn
			if ( ( this.respawnTimer + 5000 < new Date().getTime() ) && this.player.lives>0 ) {
				this.player.dead = false;
				this.player.lives--;
			}
			else  {
				if ( this.respawnTimer + 3000 < new Date().getTime() && this.player.lives==0) {
					this.context.font = '32pt Calibri';
					this.context.fillStyle = '#aaa';
    			this.context.fillText( 'GAME OVER', 300, 300 );
				}
				if ( this.respawnTimer + 5000 < new Date().getTime() ) {
					this.context.font = '22pt Calibri';
					this.context.fillStyle = '#aaa';
    			this.context.fillText( 'Press mouse to play again', 250, 322 );
					if ( inputState.mouse.down!=0 ) {
						this.player.dead = false;
						this.player.lives = 2;
						this.player.score = 0;
						this.enemyTimerInterval = startInterval;
						while (this.enemies.length > 0) {
							this.enemies.pop();
						}
					}
				}
			}
		}

		/** DEBUG INFORMATIONS **/
		this.context.font = '14pt Calibri';
		this.context.fillStyle = '#fff';
    this.context.fillText( JSON.stringify( this.player ), 20, 20);
	};

	this.setCurrentScene( play );
	window.requestAnimationFrame( this.step.bind(this) );
}

Game.prototype.step = function() {
	this.context.beginPath();
	this.context.rect(0, 0, this.width, this.height);
	this.context.fillStyle = 'black';
	this.context.fill();
	this.context.lineWidth = 1;
	this.context.strokeStyle = 'black';
	this.context.stroke();
	if ( this.currentScene!==undefined ) {
		this.currentScene.onStep();
	}
	window.requestAnimationFrame( this.step.bind(this) );
}

/** End of Game class **/

var game = new Game( { canvasName: 'canvas', width: 800, height: 600 } );
game.start();